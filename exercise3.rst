Exercise 3
===========


Please source the following file before proceeding::

   source setup.sh


In this exercise, check out each modulefiles as you proceed with the instructions to
see how ``module`` command interact with modulefiles


You can check the content of the module file using ``module --raw show modulefile`` or 
use any linux command like ``cat`` ``view`` ``vim``

Example: **module help example**
--------------------------------

.. code::

        module help a
        ml a


Example: **prereq example where module b depends on module a**
---------------------------------------------------------------

You can use ``prereq()`` function to define module prereq before loading a module. In this example we defined ``prereq("a")`` in modulefile b. 

.. code::

        ml purge
        ml a 
        ml b

        ml purge
        ml b

If you run the last command, we should see ``ml b`` fail because it has a prereq on module ``a`` 

Example: **modulefile c has conflict on module a**
--------------------------------------------------

We can define module conflicts using ``conflict()`` method which can be used to prevent two modules to be loaded at same time. In this example we defined ``conflict("a")`` in modulefile c. Note that this is a one-way conflict
and not bi-directional as we will see in example below when we load the modules.


.. code::

        ml help c
        ml whatis c

        ml purge
        ml c

        ml purge
        ml a c
        ml c a

There is a ``conflict("a")`` set in modulefile c which means when module c is loaded module a can't be loaded. 
In this case ``ml c a`` will work but ``ml a c`` will not work because the later will load ``a`` first before loading c.


Example: **Loading dependent modules.**
----------------------------------------

In this example we will have module ``d`` load modules ``a`` and ``b`` and when we unload ``d`` the dependent modules are removed as well.

.. code::

        module purge
        module load d
        module list
        module unload d
        module list

Example: **Conditional load**
------------------------------

In this example we will introduce conditional load using an **if** statement using **isloaded** method to determine if we need to load the modules. For this example see content
of modulefile ``e`` which will conditionally load ``a`` and ``b`` if they are not loaded. If we unload module ``e`` the dependent modules are not removed because the conditional is not met during unload. 

.. code::

        module purge
        module load e
        module list
        module unload e
        module list

Example: **Print message using LmodMessage example** 
--------------------------------------------------------

If you want to print message to stdout upon module operation you can use **LmodMessage** function. In this example we will print some messages when loading module f.

.. code::

        ml purge
        ml f


Example: **Declaring an alias inside modulefile**
---------------------------------------------------

We can declare alias commands in modulefile using ``set_alias`` function which will get set upon loading modules. The alias are defined in the scope of the modulefile, once ``g`` is removed the alias ``cpu`` is unset.

.. code::

        ml purge
        ml g
        cpu
        ml -g
        cpu


Example: **Introspection Function and executing Linux commands from modulefile**
----------------------------------------------------------------------------------


Lmod provides several `Introspection Functions <https://lmod.readthedocs.io/en/latest/050_lua_modulefiles.html#introspection-functions>`_ to query module name, version, hierarchy that 
can be used to write generic modules. These include **myModuleName**, **myModuleVersion**, **myModuleFullName**, etc...

.. code::

        ml purge
        ml h/1.0
        module unload h/1.0


