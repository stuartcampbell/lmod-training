Exercise 1
==========


Searching for Modules 
-----------------------

In Lmod you can search for modules using ``module avail`` and ``module spider``. 

View all modules in system::


    module av


Search for a module gcc::


    module av gcc


The `module spider <https://lmod.readthedocs.io/en/latest/135_module_spider.html>`_ command reports all modules in MODULEPATH similar to **module avail** but with more details. The difference is
``module avail`` only shows modules part of $MODULEPATH however in Lmod you can have Hierarchial Module Trees and that's where ``module spider`` comes 
into play. If you are a user who wants to know what software is installed, you should use ``module spider`` command.  Running module spider without any 
arguments will report all modules in $MODULEPATH along with any subtrees that are hidden::

   module spider

If you want to search for specific name of module, the ``module spider <name>`` will report all instances of the same module. In example below we report
all **gcc** modules::

   module spider gcc

To view a specific module version and how to load module you can run the following::

   module spider gcc/5.0


Loading and Unloading Modules
-------------------------------

Try loading a module::

        module purge
        module load gcc
        module list


Lmod has several rules when loading module files for more information see 
`here <https://lmod.readthedocs.io/en/latest/060_locating.html>`_.  In this example,
Lmod picked ``gcc/5.0`` because we have a **.version** file that sets default 
version even though we have ``gcc/6.0``.

::

        bash-4.2$ cat modules/ex2/gcc/.version 
        #%Module
        set ModulesVersion "5.0"


Similarly, with **pgi** we have two modules ``17.10`` and ``18.10``, if you run 
the following you should load ``pgi/18.10`` because it is the highest version

::

        module purge
        module load pgi
        module list


Lmod will display ``(D)`` next to default module when you run ``module av``. Alternately you can list all default modules using::

        module -d av


Try loading multiple modules::

        module purge
        module load gaussian
        module load pgi
        module list


Try loading two modules in one command::

        module purge
        module load gaussian pgi
        module list

If you did this correctly, you should see the following::

        bash-4.2$ module list

        Currently Loaded Modules:
        1) gaussian/g16.a03.avx   2) pgi/18.10 (g)

        Where:
        g:  built for GPU


Try swaping one module with another::

        module purge
        module load gaussian anaconda2
        module list
        module swap gaussian gcc
        module list

If you did this correctly you should see::

        bash-4.2$ module list

        Currently Loaded Modules:
        1) anaconda2/4.3.0   2) gcc/5.0


You can also use **sw** or **switch** to swap modules 


Try loading/unloading a module::

        module purge
        module load anaconda2 pgi
        module load gaussian
        module list
        module unload pgi
        module list


Try loading and unloading a module in one command::

        module purge
        module load anaconda2 pgi
        module load gaussian unload pgi

At this point, you should get an error as follows:: 

    Lmod has detected the following error: The following module(s) are unknown: "unload"
    
    Please check the spelling or version number. Also try "module spider ..."
    It is also possible your cache file is out-of-date; it may help to try:
      $ module --ignore-cache load "unload"
    
    Also make sure that all modulefiles written in TCL start with the string #%Module
    

Lmod thinks ``unload`` is a modulefile that needs to be loaded (i.e `module load unload`) instead of running ``module unload`` command and therefore it errors out. 
Now we will discuss the `ml <https://lmod.readthedocs.io/en/latest/010_user.html#ml-a-convenient-tool>`_ command and how it can be used to load and unload modules in same command. 
Let's try the following and see if we can load **gaussian** and unload **pgi** in single command as follows::

        module purge
        module load anaconda2 pgi
        ml gaussian -pgi
        module list

The current modules should be::

        Currently Loaded Modules:
        1) anaconda2/4.3.0   2) gaussian/g16.a03.avx


ml
-----


``ml`` is another bash function that operates similar to ``module`` but there are 
slight differences. The ``ml`` command is defined as follows::

        bash-4.2$ type ml
        ml is a function
        ml () 
        { 
        eval $($LMOD_DIR/ml_cmd "$@")
        }

        bash-4.2$ type module
        module is a function
        module () 
        { 
        eval $($LMOD_CMD bash "$@") && eval $(${LMOD_SETTARG_CMD:-:} -s sh)
        }

The command arguments for ``ml`` are passed to ``$LMOD_DIR/ml_cmd`` instead of ``$LMOD_CMD bash`` for module and these are slightly different. The `ml` and `module`
share some commands some of these commands  will be described below::

  module list <--> ml
  module avail <--> ml av
  module purge <--> ml purge
  module load gcc <--> ml gcc
  module unload gcc <--> ml -gcc
  module show gcc <--> ml show gcc
  module restore <--> ml restore
  module spider <--> ml spider
  module savelist <--> ml savelist
  module describe <--> ml describe
  module restore <--> ml restore

Let's try a few examples using module and ml command.

Try listing modules using ``ml`` and ``module list``. By default ``ml`` will list active modules::

      module purge
      module load gcc
      ml
      module list

Let's loading modules using ``ml``::

        module purge
        ml gcc
        ml 


The ``ml purge`` works similar to ``module purge``, lets purge and load some modules and purge again::

        ml purge
        ml
        ml gcc
        ml purge
        ml 

It's good practice to purge your modules in your job script before loading modules of interest since startup modules may change overtime.
The ``module purge`` also provides a starting point to clear any user environment issues, if your site has setup startup modules upon login ``module restore``
is good way to restore your user environment back to original setup. 
